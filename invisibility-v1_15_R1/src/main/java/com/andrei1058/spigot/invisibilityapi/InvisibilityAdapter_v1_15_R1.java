package com.andrei1058.spigot.invisibilityapi;

import net.minecraft.server.v1_15_R1.*;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class InvisibilityAdapter_v1_15_R1 implements InvisibilityAPI {

    protected static InvisibilityAdapter_v1_15_R1 INSTANCE;
    protected final InvisibilityTracker INVISIBILITY_TRACKER;

    public InvisibilityAdapter_v1_15_R1() {
        if (INSTANCE == null) {
            INSTANCE = this;
        }
        INVISIBILITY_TRACKER = new InvisibilityTracker();
    }

    @Override
    public void hidePlayerToPlayer(Player playerToBeHidden, Player to, boolean hideInTab) {
        if (playerToBeHidden == null) return;
        if (to == null) return;
        if (to.equals(playerToBeHidden)) return;
        if (canSee(to.getUniqueId(), playerToBeHidden.getUniqueId())) {

            if (hideInTab) {
                PacketPlayOutPlayerInfo hide = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, ((CraftPlayer) playerToBeHidden).getHandle());
                EntityPlayer boundTo = ((CraftPlayer) to).getHandle();
                boundTo.playerConnection.sendPacket(hide);
            }
            if (to.getWorld().equals(playerToBeHidden.getWorld())) {
                PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(((CraftPlayer) playerToBeHidden).getHandle().getId());
                EntityPlayer boundTo = ((CraftPlayer) to).getHandle();
                boundTo.playerConnection.sendPacket(destroy);
            }

            INVISIBILITY_TRACKER.addHidden(playerToBeHidden, to);
        }
    }

    @Override
    public void unHidePlayerForPlayer(Player playerToBeShown, Player to) {
        if (playerToBeShown == null) return;
        if (to == null) return;
        if (to.equals(playerToBeShown)) return;
        if (!canSee(to.getUniqueId(), playerToBeShown.getUniqueId())) {
            INVISIBILITY_TRACKER.removeHidden(playerToBeShown, to);

            EntityPlayer entityPlayer = ((CraftPlayer) playerToBeShown).getHandle();
            EntityPlayer boundTo = ((CraftPlayer) to).getHandle();
            boundTo.playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));


            if (to.getWorld().equals(playerToBeShown.getWorld())) {
                int renderDistance = INVISIBILITY_TRACKER.getRenderDistance(playerToBeShown.getWorld().getName());

                PacketPlayOutNamedEntitySpawn show = new PacketPlayOutNamedEntitySpawn(entityPlayer);
                PacketPlayOutEntityVelocity playerVelocity = new PacketPlayOutEntityVelocity(entityPlayer);

                PacketPlayOutEntityEquipment hand1 = null;
                PacketPlayOutEntityEquipment hand2 = null;
                PacketPlayOutEntityEquipment helmet = null;
                PacketPlayOutEntityEquipment chest = null;
                PacketPlayOutEntityEquipment pants = null;
                PacketPlayOutEntityEquipment boots = null;

                if (!INVISIBILITY_TRACKER.hasHiddenArmor(playerToBeShown.getUniqueId())) {
                    hand1 = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.MAINHAND, entityPlayer.getItemInMainHand());
                    hand2 = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.OFFHAND, entityPlayer.getItemInOffHand());
                    helmet = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.HEAD, entityPlayer.inventory.getArmorContents().get(3));
                    chest = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.CHEST, entityPlayer.inventory.getArmorContents().get(2));
                    pants = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.LEGS, entityPlayer.inventory.getArmorContents().get(1));
                    boots = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.FEET, entityPlayer.inventory.getArmorContents().get(0));

                }

                if (playerToBeShown.getLocation().distance(to.getLocation()) <= renderDistance) {
                    boundTo.playerConnection.sendPacket(show);
                    boundTo.playerConnection.sendPacket(playerVelocity);
                    if (hand1 != null) {
                        for (Packet<?> packet : new Packet[]{hand1, hand2, helmet, chest, pants, boots}) {
                            boundTo.playerConnection.sendPacket(packet);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void hidePlayerToPlayers(Player target, List<Player> to, boolean hideInTab) {
        if (target == null) return;
        if (to == null) return;
        if (to.isEmpty()) return;
        PacketPlayOutPlayerInfo hide = null;
        if (hideInTab) {
            hide = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, ((CraftPlayer) target).getHandle());
        }
        PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(((CraftPlayer) target).getHandle().getId());

        for (Player p : to) {
            if (p.equals(target)) continue;
            if (canSee(p.getUniqueId(), target.getUniqueId())) {
                EntityPlayer boundTo = ((CraftPlayer) p).getHandle();
                if (hide != null) {
                    boundTo.playerConnection.sendPacket(hide);
                }
                if (!p.getWorld().equals(target.getWorld())) continue;
                boundTo.playerConnection.sendPacket(destroy);
            }
        }
        INVISIBILITY_TRACKER.addHidden(target, to);
    }

    @Override
    public void unHidePlayerForPlayers(Player target, List<Player> to) {
        if (target == null) return;
        if (to == null) return;
        if (to.isEmpty()) return;
        List<Player> players = to.stream().filter(p -> !canSee(p.getUniqueId(), target.getUniqueId())).collect(Collectors.toList());
        if (players.isEmpty()) return;
        INVISIBILITY_TRACKER.removeHidden(target, players);

        int renderDistance = INVISIBILITY_TRACKER.getRenderDistance(target.getWorld().getName());

        EntityPlayer entityPlayer = ((CraftPlayer) target).getHandle();
        PacketPlayOutNamedEntitySpawn show = new PacketPlayOutNamedEntitySpawn(entityPlayer);
        PacketPlayOutPlayerInfo playerInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer);
        PacketPlayOutEntityVelocity playerVelocity = new PacketPlayOutEntityVelocity(entityPlayer);

        PacketPlayOutEntityEquipment hand1 = null;
        PacketPlayOutEntityEquipment hand2 = null;
        PacketPlayOutEntityEquipment helmet = null;
        PacketPlayOutEntityEquipment chest = null;
        PacketPlayOutEntityEquipment pants = null;
        PacketPlayOutEntityEquipment boots = null;

        if (!INVISIBILITY_TRACKER.hasHiddenArmor(target.getUniqueId())) {
            hand1 = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.MAINHAND, entityPlayer.getItemInMainHand());
            hand2 = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.OFFHAND, entityPlayer.getItemInOffHand());
            helmet = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.HEAD, entityPlayer.inventory.getArmorContents().get(3));
            chest = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.CHEST, entityPlayer.inventory.getArmorContents().get(2));
            pants = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.LEGS, entityPlayer.inventory.getArmorContents().get(1));
            boots = new PacketPlayOutEntityEquipment(entityPlayer.getId(), EnumItemSlot.FEET, entityPlayer.inventory.getArmorContents().get(0));

        }

        for (Player p : players) {
            if (p == null) continue;
            if (p.equals(target)) continue;
            EntityPlayer boundTo = ((CraftPlayer) p).getHandle();
            boundTo.playerConnection.sendPacket(playerInfo);
            if (p.getWorld().equals(target.getWorld())) {
                if (target.getLocation().distance(p.getLocation()) <= renderDistance) {
                    boundTo.playerConnection.sendPacket(show);
                    boundTo.playerConnection.sendPacket(playerVelocity);
                    if (hand1 != null) {
                        for (Packet<?> packet : new Packet[]{hand1, hand2, helmet, chest, pants, boots}) {
                            boundTo.playerConnection.sendPacket(packet);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void hidePlayersToPlayer(List<Player> toBeHidden, Player playerReceiver, boolean hideInTab) {
        if (toBeHidden == null) return;
        if (playerReceiver == null) return;
        if (toBeHidden.isEmpty()) return;

        EntityPlayer boundTo = ((CraftPlayer) playerReceiver).getHandle();

        for (Player toHide : toBeHidden) {
            if (toHide.equals(playerReceiver)) continue;
            if (canSee(playerReceiver.getUniqueId(), toHide.getUniqueId())) {
                EntityPlayer playerToHide = ((CraftPlayer) toHide).getHandle();
                if (hideInTab) {
                    boundTo.playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, playerToHide));
                }
                if (toHide.getWorld().equals(playerReceiver.getWorld())) {
                    boundTo.playerConnection.sendPacket(new PacketPlayOutEntityDestroy(playerToHide.getId()));
                }
            }
            INVISIBILITY_TRACKER.addHidden(toHide, playerReceiver);
        }
    }

    @Override
    public void unHidePlayersForPlayer(List<Player> toBeShown, Player playerReceiver) {
        if (playerReceiver == null) return;
        if (toBeShown == null) return;
        if (toBeShown.isEmpty()) return;

        List<Player> players = toBeShown.stream().filter(p -> !canSee(playerReceiver.getUniqueId(), p.getUniqueId())).collect(Collectors.toList());
        if (players.isEmpty()) return;

        int renderDistance = INVISIBILITY_TRACKER.getRenderDistance(playerReceiver.getWorld().getName());

        EntityPlayer receiver = ((CraftPlayer) playerReceiver).getHandle();

        for (Player showPlayer : players) {
            INVISIBILITY_TRACKER.removeHidden(showPlayer, playerReceiver);
            if (showPlayer == null) continue;
            if (showPlayer.equals(playerReceiver)) continue;

            EntityPlayer toShow = ((CraftPlayer) showPlayer).getHandle();

            receiver.playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, toShow));

            if (showPlayer.getWorld().equals(playerReceiver.getWorld())) {
                if (playerReceiver.getLocation().distance(showPlayer.getLocation()) <= renderDistance) {
                    receiver.playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(toShow));
                    receiver.playerConnection.sendPacket(new PacketPlayOutEntityVelocity(toShow));
                    if (!INVISIBILITY_TRACKER.hasHiddenArmor(showPlayer.getUniqueId())) {
                        receiver.playerConnection.sendPacket(new PacketPlayOutEntityEquipment(toShow.getId(), EnumItemSlot.MAINHAND, toShow.getItemInMainHand()));
                        receiver.playerConnection.sendPacket(new PacketPlayOutEntityEquipment(toShow.getId(), EnumItemSlot.OFFHAND, toShow.getItemInOffHand()));
                        receiver.playerConnection.sendPacket(new PacketPlayOutEntityEquipment(toShow.getId(), EnumItemSlot.HEAD, toShow.inventory.getArmorContents().get(3)));
                        receiver.playerConnection.sendPacket(new PacketPlayOutEntityEquipment(toShow.getId(), EnumItemSlot.CHEST, toShow.inventory.getArmorContents().get(2)));
                        receiver.playerConnection.sendPacket(new PacketPlayOutEntityEquipment(toShow.getId(), EnumItemSlot.LEGS, toShow.inventory.getArmorContents().get(1)));
                        receiver.playerConnection.sendPacket(new PacketPlayOutEntityEquipment(toShow.getId(), EnumItemSlot.FEET, toShow.inventory.getArmorContents().get(0)));
                    }
                }
            }
        }
    }

    @Override
    public boolean canSee(UUID player1, UUID player2) {
        return INVISIBILITY_TRACKER.canSee(player1, player2);
    }

    @Override
    public void registerOnLogin(Player player) {
        if (((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().get("InvisibilityAPI") == null) {
            ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline().addBefore("packet_handler", " InvisibilityAPI", new PacketListener_v1_15_R1(player.getUniqueId()));
        }
    }

    @Override
    public void unregisterOnLeave(Player player) {
        if (player == null) return;
        INVISIBILITY_TRACKER.onLeave(player.getUniqueId());
    }

    @Override
    public void showArmor(Player armorHolder) {

        EntityPlayer holder = ((CraftPlayer) armorHolder).getHandle();
        PacketPlayOutEntityEquipment hand1 = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.MAINHAND, holder.getItemInMainHand());
        PacketPlayOutEntityEquipment hand2 = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.OFFHAND, holder.getItemInOffHand());
        PacketPlayOutEntityEquipment helmet = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.HEAD, holder.inventory.getArmorContents().get(3));
        PacketPlayOutEntityEquipment chest = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.CHEST, holder.inventory.getArmorContents().get(2));
        PacketPlayOutEntityEquipment pants = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.LEGS, holder.inventory.getArmorContents().get(1));
        PacketPlayOutEntityEquipment boots = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.FEET, holder.inventory.getArmorContents().get(0));

        for (Player p : armorHolder.getWorld().getPlayers()) {
            if (p.equals(armorHolder)) continue;
            PlayerConnection playerConnection = ((CraftPlayer) p).getHandle().playerConnection;
            playerConnection.sendPacket(boots);
            playerConnection.sendPacket(pants);
            playerConnection.sendPacket(chest);
            playerConnection.sendPacket(helmet);
            playerConnection.sendPacket(hand1);
            playerConnection.sendPacket(hand2);
        }
        INVISIBILITY_TRACKER.addHiddenArmor(armorHolder.getUniqueId());
    }

    @Override
    public void hideArmor(Player armorHolder) {
        INVISIBILITY_TRACKER.removeHiddenArmor(armorHolder.getUniqueId());

        EntityPlayer holder = ((CraftPlayer) armorHolder).getHandle();
        PacketPlayOutEntityEquipment hand1 = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.MAINHAND, new ItemStack(Item.getById(0)));
        PacketPlayOutEntityEquipment hand2 = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.OFFHAND, new ItemStack(Item.getById(0)));
        PacketPlayOutEntityEquipment helmet = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.HEAD, new ItemStack(Item.getById(0)));
        PacketPlayOutEntityEquipment chest = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.CHEST, new ItemStack(Item.getById(0)));
        PacketPlayOutEntityEquipment pants = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.LEGS, new ItemStack(Item.getById(0)));
        PacketPlayOutEntityEquipment boots = new PacketPlayOutEntityEquipment(holder.getId(), EnumItemSlot.FEET, new ItemStack(Item.getById(0)));

        for (Player p : armorHolder.getWorld().getPlayers()) {
            if (p.equals(armorHolder)) continue;
            PlayerConnection playerConnection = ((CraftPlayer) p).getHandle().playerConnection;
            playerConnection.sendPacket(boots);
            playerConnection.sendPacket(pants);
            playerConnection.sendPacket(chest);
            playerConnection.sendPacket(helmet);
            playerConnection.sendPacket(hand1);
            playerConnection.sendPacket(hand2);
        }
    }
}
