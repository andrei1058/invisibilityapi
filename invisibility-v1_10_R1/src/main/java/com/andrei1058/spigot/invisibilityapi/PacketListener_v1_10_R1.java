package com.andrei1058.spigot.invisibilityapi;

import com.mojang.authlib.GameProfile;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_10_R1.EntityPlayer;
import net.minecraft.server.v1_10_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_10_R1.PlayerList;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PacketListener_v1_10_R1 extends ChannelDuplexHandler {

    private final UUID player;

    public PacketListener_v1_10_R1(UUID player) {
        this.player = player;
    }

    @Override
    public void write(ChannelHandlerContext channelHandlerContext, Object o, ChannelPromise channelPromise) throws Exception {
        if (o instanceof PacketPlayOutNamedEntitySpawn) {
            UUID playerUUID = getFieldValue(o, "b");

            if (!InvisibilityAdapter_v1_10_R1.INSTANCE.canSee(player, playerUUID)) {
                return;
            }
        } else if (o instanceof PacketPlayOutPlayerInfo) {
            PacketPlayOutPlayerInfo.EnumPlayerInfoAction action = getFieldValue(o, "a");
            if (action == PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER) {
                List<PacketPlayOutPlayerInfo.PlayerInfoData> list = getFieldValue(o, "b");

                if (list.stream().anyMatch(data -> !InvisibilityAdapter_v1_10_R1.INSTANCE.canSee(player, data.a().getId()))) {
                    // if list has a single player just cancel the packet
                    if (list.size() == 1) return;

                    // create a fresh list with the players he can see only
                    List<UUID> newList = new ArrayList<>();
                    for (PacketPlayOutPlayerInfo.PlayerInfoData data : list) {
                        GameProfile profile = data.a();
                        if (InvisibilityAdapter_v1_10_R1.INSTANCE.canSee(player, profile.getId())) {
                            newList.add(profile.getId());
                        }
                    }

                    EntityPlayer[] replacement = new EntityPlayer[newList.size()];
                    PlayerList playerList = ((CraftServer) Bukkit.getServer()).getHandle();
                    for (int i = 0; i < newList.size(); i++) {
                        replacement[i] = playerList.a(newList.get(i));
                    }
                    super.write(channelHandlerContext, new PacketPlayOutPlayerInfo(action, replacement), channelPromise);
                    return;
                }
            }
        }
        super.write(channelHandlerContext, o, channelPromise);
    }


    @SuppressWarnings("unchecked")
    public static <T> T getFieldValue(Object instance, String fieldName) throws Exception {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T) field.get(instance);
    }
}
