[![Discord Server](https://discord.com/api/guilds/201345265821679617/widget.png)](https://discord.gg/XdJfN2X)

### Description

This is a simple library/ util or however you want to call it that is used to vanish spectators in mini-games. It can be used to hide re-spawning players as well with the option to keep their names in TAB.

### Supported versions

* 1.8 R3
* 1.9 R2
* 1.10 R1
* 1.11 R1
* 1.12 R1
* 1.13 R2
* 1.14 R1
* 1.15 R1
* 1.16 R2

### Maven
```xml
<repositories>
    <repository>
        <id>codemc-repo</id>
        <url>https://repo.codemc.io/repository/maven-public/</url>
    </repository>
</repositories>
```
```xml
<dependencies>
    <dependency>
        <groupId>com.andrei1058.spigot.invisibilityapi</groupId>
        <artifactId>invisibility-api</artifactId>
        <version>VERSION HERE</version>
        <scope>compile</scope>
    </dependency>
</dependencies>
```

### Alternative Maven
```xml
<repositories>
    <repository>
        <id>andrei1058-releases</id>
        <url>https://repo.andrei1058.com/releases/</url>
    </repository>
</repositories>
```
```xml
<dependencies>
    <dependency>
        <groupId>com.andrei1058.spigot.invisibilityapi</groupId>
        <artifactId>invisibility-api</artifactId>
        <version>VERSION HERE</version>
        <scope>compile</scope>
    </dependency>
</dependencies>
```