package com.andrei1058.spigot.invisibilityapi;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.UUID;

public interface InvisibilityAPI {

    /**
     * Hide a player to another player.
     *
     * @param playerToBeHidden player to be hidden.
     * @param playerReceiver   the player who won't see the other player.
     * @param hideInTab        hide player in player list.
     */
    void hidePlayerToPlayer(Player playerToBeHidden, Player playerReceiver, boolean hideInTab);

    /**
     * Show a hidden player to a target player.
     *
     * @param playerToBeShown the player hidden before.
     * @param target          the player who will be able to see the other player again.
     */
    void unHidePlayerForPlayer(Player playerToBeShown, Player target);

    /**
     * Hide a player to another player.
     *
     * @param playerToBeHidden player to be hidden.
     * @param receivers        the players who won't see the other player.
     * @param hideInTab        hide player from player list.
     */
    void hidePlayerToPlayers(Player playerToBeHidden, List<Player> receivers, boolean hideInTab);

    /**
     * Show a hidden player to a target player list.
     *
     * @param playerToBeShown the player hidden before.
     * @param receivers       the players who will be able to see the other player again.
     */
    void unHidePlayerForPlayers(Player playerToBeShown, List<Player> receivers);

    /**
     * Hide a list of players to a player.
     *
     * @param playerReceiver the player who will receive the hide packets.
     * @param toBeHidden        the players to be hidden from player1.
     * @param hideInTab        hide players in player list.
     */
    void hidePlayersToPlayer(List<Player> toBeHidden, Player playerReceiver, boolean hideInTab);

    /**
     * Show a list of hidden players to a target player.
     *
     * @param playerReceiver the player who will be able to see the players again.
     * @param toBeShown       the players to be shown.
     */
    void unHidePlayersForPlayer(List<Player> toBeShown, Player playerReceiver);

    /**
     * Check if player1 can see player2.
     *
     * @param player1 check if this player can see player2.
     * @param player2 check if this player can be seen by player1.
     * @return true if player1 can see player2.
     */
    boolean canSee(UUID player1, UUID player2);

    /**
     * If you want this lib to work it is a must to use this method when a player joins.
     * This will register a packet handler for the joining player.
     *
     * @param player affected player.
     */
    void registerOnLogin(Player player);

    /**
     * This must be called when a player leaves the server
     * to remove its data.
     *
     * @param player player to be removed.
     */
    void unregisterOnLeave(Player player);

    /**
     * Hide a player armor.
     *
     * @param armorHolder player armorHolder;
     */
    void hideArmor(Player armorHolder);

    /**
     * Show a player armor.
     *
     * @param armorHolder player armorHolder;
     */
    void showArmor(Player armorHolder);


    /**
     * Load support for your current server version.
     *
     * @return instance for your server version.
     */
    @Nullable
    static InvisibilityAPI load() {
        String version = Bukkit.getServer().getClass().getName().split("\\.")[3];
        Class<?> c;
        try {
            c = Class.forName("com.andrei1058.spigot.invisibilityapi.InvisibilityAdapter_" + version);
        } catch (ClassNotFoundException e) {
            //I can't run on your version
            return null;
        }
        try {
            return (InvisibilityAPI) c.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }
}
