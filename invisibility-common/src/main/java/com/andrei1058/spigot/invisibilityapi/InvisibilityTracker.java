package com.andrei1058.spigot.invisibilityapi;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

class InvisibilityTracker {

    // hidden player to players
    private static final ConcurrentHashMap<UUID, LinkedList<UUID>> playerToPlayers = new ConcurrentHashMap<>();
    private static final LinkedList<UUID> hiddenArmor = new LinkedList<>();

    /**
     * Check if player1 can see player2.
     *
     * @param player1 check if this player can see player2.
     * @param player2 check if this player can be seen by player1.
     * @return true if player1 can see player2.
     */
    protected boolean canSee(UUID player1, UUID player2) {
        if (playerToPlayers.containsKey(player2)) {
            return !playerToPlayers.get(player2).contains(player1);
        }
        return true;
    }

    /**
     * Add a hidden player.
     *
     * @param hidden    the hidden player.
     * @param receivers players who won't see the player.
     */
    protected void addHidden(Player hidden, Player... receivers) {
        if (hidden == null) return;
        if (receivers == null) return;
        if (receivers.length == 0) return;

        LinkedList<UUID> list = playerToPlayers.get(hidden.getUniqueId());

        if (list == null) {
            list = new LinkedList<>();
            for (Player p : receivers) {
                if (p == null) continue;
                if (p.equals(hidden)) continue;
                list.add(p.getUniqueId());
            }
            playerToPlayers.put(hidden.getUniqueId(), list);
        } else {
            for (Player p : receivers) {
                if (p == null) continue;
                if (p.equals(hidden)) continue;
                if (!list.contains(p.getUniqueId())) {
                    list.add(p.getUniqueId());
                }
            }

            playerToPlayers.replace(hidden.getUniqueId(), list);
        }
    }

    /**
     * Add a hidden player.
     *
     * @param hidden    the hidden player.
     * @param receivers players who won't see the player.
     */
    protected void addHidden(Player hidden, List<Player> receivers) {
        if (hidden == null) return;
        if (receivers == null) return;
        if (receivers.isEmpty()) return;

        LinkedList<UUID> list = playerToPlayers.get(hidden.getUniqueId());

        if (list == null) {
            list = new LinkedList<>();
            for (Player p : receivers) {
                if (p == null) continue;
                if (p.equals(hidden)) continue;
                list.add(p.getUniqueId());
            }
            playerToPlayers.put(hidden.getUniqueId(), list);
        } else {
            for (Player p : receivers) {
                if (p == null) continue;
                if (p.equals(hidden)) continue;
                if (!list.contains(p.getUniqueId())) {
                    list.add(p.getUniqueId());
                }
            }

            playerToPlayers.replace(hidden.getUniqueId(), list);
        }
    }

    /**
     * Add a hidden player.
     *
     * @param toBeShown the hidden player.
     * @param toReceive players who will see the player.
     */
    protected void removeHidden(Player toBeShown, Player... toReceive) {
        if (toBeShown == null) return;
        if (toReceive == null) return;
        if (toReceive.length == 0) return;

        LinkedList<UUID> list = playerToPlayers.get(toBeShown.getUniqueId());

        if (list != null) {
            for (Player p : toReceive) {
                if (p == null) continue;
                list.remove(p.getUniqueId());
            }

            if (list.isEmpty()) {
                playerToPlayers.remove(toBeShown.getUniqueId());
            } else {
                playerToPlayers.replace(toBeShown.getUniqueId(), list);
            }
        }
    }

    /**
     * Add a hidden player.
     *
     * @param hidden    the hidden player.
     * @param receivers players who will see the player.
     */
    protected void removeHidden(Player hidden, List<Player> receivers) {
        if (hidden == null) return;
        if (receivers == null) return;
        if (receivers.isEmpty()) return;

        LinkedList<UUID> list = playerToPlayers.get(hidden.getUniqueId());

        if (list != null) {
            for (Player p : receivers) {
                if (p == null) continue;
                list.remove(p.getUniqueId());
            }

            if (list.isEmpty()) {
                playerToPlayers.remove(hidden.getUniqueId());
            } else {
                playerToPlayers.replace(hidden.getUniqueId(), list);
            }
        }
    }

    protected void onLeave(UUID player) {
        playerToPlayers.forEach((u, l) -> {
            l.removeIf(u2 -> u2.equals(player));
            if (l.isEmpty()) {
                playerToPlayers.remove(u);
            }
        });
        hiddenArmor.remove(player);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    protected boolean hasHiddenArmor(UUID player) {
        return hiddenArmor.contains(player);
    }

    protected void addHiddenArmor(UUID player) {
        if (!hasHiddenArmor(player)) {
            hiddenArmor.add(player);
        }
    }

    protected void removeHiddenArmor(UUID player) {
        hiddenArmor.remove(player);
    }

    protected int getRenderDistance(String world){
        YamlConfiguration yaml = YamlConfiguration.loadConfiguration(new File("spigot.yml"));
        return yaml.get("world-settings." + world + ".entity-tracking-range.players") == null ?
                yaml.getInt("world-settings.default.entity-tracking-range.players") : yaml.getInt("world-settings." + world + ".entity-tracking-range.players");
    }
}
